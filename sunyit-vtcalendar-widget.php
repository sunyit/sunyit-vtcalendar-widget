<?php
/*
Plugin Name: SUNYIT VTCalendar Widget
Plugin URI: https://bitbucket.org/sunyit/sunyit-event-calendar-month-view-widget
Description: A simple month-view calendar with dates highlighted from SUNYIT's event calendar
Version: 0.1.2
Author: Zac Wasielewski
Author URI: http://wasielewski.org
License: GPL2

Copyright 2014 Zac Wasielewski (email: zac@wasielewski.org)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

class SUNYIT_VTCalendar_Widget extends WP_Widget {
	
	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/
	
	/**
	 * The widget constructor. Specifies the classname and description, instantiates
	 * the widget, loads localization files, and includes necessary scripts and
	 * styles.
	 */
	public function __construct() {
	
		load_plugin_textdomain( 'sunyit_vtcalendar_widget', false, plugin_dir_path( __FILE__ ) . '/lang/' );
		
		parent::__construct(
			'sunyit_vtcalendar_widget',
			'SUNYIT_VTCalendar_Widget',
			array(
				'classname'		=>	'sunyit_vtcalendar_widget',
				'description'	=>	__( 'A simple month-view calendar with days highlighted from an RSS feed source.', 'sunyit_vtcalendar_widget-locale' )
			)
		);
		
		// Load JavaScript and stylesheets
		$this->register_scripts_and_styles();
	
	} // end constructor

	/*--------------------------------------------------*/
	/* API Functions
	/*--------------------------------------------------*/
	
	/**
	 * Outputs the content of the widget.
	 *
	 * @args			The array of form elements
	 * @instance		The current instance of the widget
	 */
	function widget( $args, $instance ) {
	
		extract( $args, EXTR_SKIP );
		
		if (isset($before_widget)) echo $before_widget;
		
		$title = empty($instance['title'])
				? '' : apply_filters('title', $instance['title']);

		$year = empty($instance['year'])
				? date('Y') : apply_filters('year', $instance['year']);
				
		$month = empty($instance['month'])
				? date('m') : apply_filters('month', $instance['month']);

		$url = empty($instance['url'])
				? '' : apply_filters('url', $instance['url']);
		
		if ($url) {
			$items = $this->fetch_feed_items($url);
			$days = $this->get_days_for_month_from_feed_items($year,$month,$items);
		} else {
			$days = array();
		}
		
		$calendar = $this->draw_calendar($year,$month,$days);

		if ( $title ) {
				echo (isset($before_title) ? $before_title : '') . $title . (isset($after_title) ? $after_title : '');
		}
    
		include( plugin_dir_path(__FILE__) . '/views/widget.php' );
		
		if (isset($after_widget)) echo $after_widget;
		
	} // end widget

	/**
	 * Outputs the content of the list widget.
	 *
	 * @args			The array of form elements
	 * @instance		The current instance of the widget
	 */
	function list_widget( $args, $instance ) {
	
		extract( $args, EXTR_SKIP );
		
		if (isset($before_widget)) echo $before_widget;
		
		$title = empty($instance['title'])
				? '' : apply_filters('title', $instance['title']);
		$url = empty($instance['url'])
				? '' : html_entity_decode(apply_filters('url', $instance['url']));
		$limit = empty($instance['limit'])
				? 5 : apply_filters('limit', $instance['limit']);    
		$template = empty($instance['template'])
				? $this->list_widget_template() : apply_filters('template', $instance['template']);

		date_default_timezone_set('America/New_York');
		$feed = fetch_feed_with_options(array(
			'url' => $url,
			'num' => $limit,
			'simplepie_enable_order_by_date' => false
		));
		
		$items = array();
		foreach ($feed as $item):
		  $items[] = array(
		    'date' => date("n/j",strtotime($item->get_date())),
		    'link' => $item->get_permalink(),
		    'title' => $item->get_title(),
		    'description' => $item->get_description(),
		  );
		endforeach;
				
    require_once( plugin_dir_path( __FILE__ ) . '/vendor/mustache.php' );
    $m = new Mustache_Engine;
    $list_html = $m->render($template, array('items' => $items));
		
		$html = '';
		
		if ( $title ) {
				$html .= (isset($before_title) ? $before_title : '') . $title . (isset($after_title) ? $after_title : '');
		}
    
		$html .= $list_html;
		
		if (isset($after_widget)) {
		   $html .= $after_widget;
		}
		
		return $html;
		
	} // end widget
	
	private function list_widget_template() {
    	return file_get_contents( plugin_dir_path(__FILE__) . '/views/list.handlebars.html' );
	}
	
	/**
	 * Processes the widget's options to be saved.
	 *
	 * @new_instance	The previous instance of values before the update.
	 * @old_instance	The new instance of values to be generated via the update.
	 */
	function update( $new_instance, $old_instance ) {
		
		$instance = $old_instance;
		
        $instance['title'] = strip_tags(stripslashes($new_instance['title']));
        $instance['year'] = strip_tags(stripslashes($new_instance['year']));
        $instance['month'] = strip_tags(stripslashes($new_instance['month']));
        
        if (!is_array($instance['days'])) {
            $instance['days'] = array();
        }
        foreach ($new_instance['days'] as $k=>$v) {
            $instance['days'][$k] = strip_tags(stripslashes($v));
        }
    
		return $instance;
		
	} // end widget
	
	/**
	 * Generates the administration form for the widget.
	 *
	 * @instance	The array of keys and values for the widget.
	 */
	function form( $instance ) {
	    
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title'	=>	'',
				'year'	=>	date("Y"),
				'month'	=>	date("n"),
				'days'	=>	array(),
			)
		);
		
        $title = strip_tags(stripslashes($instance['title']));
        $year  = strip_tags(stripslashes($instance['year']));
        $month = strip_tags(stripslashes($instance['month']));
        
        if (is_array($instance['days'])) {
            $days = $instance['days'];
            foreach ($instance['days'] as $k=>$v) {
                $days[$k] = strip_tags(stripslashes($v));
            }
        } else {
            $days = strip_tags(stripslashes($instance['days']));
        }
		
		// Display the admin form
    	include( plugin_dir_path(__FILE__) . '/views/admin.php' );
		
	} // end form
	
	/*--------------------------------------------------*/
	/* Private Functions
	/*--------------------------------------------------*/
  
	/**
	 * Registers and enqueues stylesheets for the administration panel and the
	 * public facing site.
	 */
	private function register_scripts_and_styles() {
	
		if ( is_admin() ) {
		
			//$this->load_file( 'sunyit_vtcalendar_widget-admin-script', '/js/admin.js', true );
			$this->load_file( 'sunyit_vtcalendar_widget-admin-style', '/css/admin.css' );
			
		} else { 
		
			//$this->load_file( 'sunyit_vtcalendar_widget-script', '/js/widget.js', true );
			$this->load_file( 'sunyit_vtcalendar_widget-style', '/css/widget.css' );
			
		} // end if/else
		
	} // end register_scripts_and_styles

	/**
	 * Helper function for registering and enqueueing scripts and styles.
	 *
	 * @name	        The ID to register with WordPress
	 * @file_path		The path to the actual file
	 * @is_script		Optional argument for if the incoming file_path is a JavaScript source file.
	 */
	private function load_file( $name, $file_path, $is_script = false ) {
		
		$url = plugins_url( $file_path, __FILE__ ) ;
		$file = plugin_dir_path( __FILE__ ) . $file_path;

		if( file_exists( $file ) ) {
		
			if( $is_script ) {
			
				wp_register_script( $name, $url, array( 'jquery' ) );
				wp_enqueue_script( $name );
				
			} else {
			
				wp_register_style( $name, $url );
				wp_enqueue_style( $name );
				
			} // end if
			
		} // end if
    
	} // end load_file

	/**
	 * Retrieves items from an RSS feed
	 *
	 * @year        The calendar year to display
	 * @month   	The calendar month to display
	 * @days		Optional array of days to highlight on the calendar
	 */
	private function fetch_feed_items( $url ) {
		$feed = fetch_feed($url);
		return $feed->get_items();
	}

	private function get_days_for_month_from_feed_items ( $year, $month, $items ) {

		$days = array();
		
		foreach ( $items as $item ) {
			$date = getdate(strtotime($item->get_date()));
			if (!( $date['year']==intval($year) && $date['mon']==intval($month) )) continue;
			$days[] = $date['mday'];
		}
		return $days;

	}
	
	private function calendar_day_link ($year,$month,$day) {
		$url = 'http://www.sunyit.edu/apps/calendar/main.php?view=day&timebegin='.date('Y-m-d',mktime(0,0,0,$month,$day,$year)).'+00:00:00';
		return '<a href="'. $url .'">' . $day .'</a>';
	}
		
	/**
	 * Generates and returns the calendar markup
	 *
	 * @year        The calendar year to display
	 * @month   	The calendar month to display
	 * @days		Optional array of days to highlight on the calendar
	 */
	private function draw_calendar( $year, $month, $days = array() ) {
    
        /* draw table */
        $calendar = '';
        $calendar .= '<table class="sunyit_vtcalendar_widget-calendar">';
        $calendar .= '<caption>'. date('F Y',mktime(0,0,0,$month,1,$year)) .'</caption>';
        $calendar .= '<tbody>';
        
        $show_headings = true;
        
        /* table headings */
        if ($show_headings==true) {
            //$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
            $headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
            $calendar.= '<tr class="sunyit_vtcalendar_widget-row"><td class="sunyit_vtcalendar_widget-day-head">'.implode('</td><td class="sunyit_vtcalendar_widget-day-head">',$headings).'</td></tr>';
        }
        
        /* days and weeks vars now ... */
        $running_day = date('w',mktime(0,0,0,$month,1,$year));
        $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
        $days_in_this_week = 1;
        $day_counter = 0;
        $dates_array = array();
        
        /* row for week one */
        $calendar.= '<tr class="sunyit_vtcalendar_widget-row">';
        
        /* print "blank" days until the first of the current week */
        for($x = 0; $x < $running_day; $x++):
            $calendar.= '<td class="sunyit_vtcalendar_widget-day-np">&nbsp;</td>';
            $days_in_this_week++;
        endfor;
        
        /* keep going with days.... */
        for($list_day = 1; $list_day <= $days_in_month; $list_day++):
            
            if ( in_array( $list_day, $days )) {
                $calendar.= '<td class="sunyit_vtcalendar_widget-day sunyit_vtcalendar_widget-highlight">';
            } else {
                $calendar.= '<td class="sunyit_vtcalendar_widget-day">';
            }
        
            /* add in the day number */
						$calendar.= '<div class="day-number">' . $this->calendar_day_link($year,$month,$list_day) . '</div>';
            
            /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
            //$calendar.= str_repeat('<p>&nbsp;</p>',2);
            
            $calendar.= '</td>';
            if($running_day == 6):
                $calendar.= '</tr>';
                if(($day_counter+1) != $days_in_month):
                    $calendar.= '<tr class="sunyit_vtcalendar_widget-row">';
                endif;
                $running_day = -1;
                $days_in_this_week = 0;
            endif;
            $days_in_this_week++; $running_day++; $day_counter++;
        endfor;
            
        /* finish the rest of the days in the week */
        if($days_in_this_week < 8):
            for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                $calendar.= '<td class="sunyit_vtcalendar_widget-day-np">&nbsp;</td>';
            endfor;
        endif;
        
        /* final row */
        $calendar.= '</tr>';
        
        /* end the table */
        $calendar.= '</tbody>';
        $calendar.= '</table>';
        
        /* all done, return result */
        return $calendar;

	}
	
} // end class

add_action( 'widgets_init', create_function( '', 'register_widget("SUNYIT_VTCalendar_Widget");' ) );

function sunyit_vtcalendar_shortcode ( $options=array() ) {

	extract(shortcode_atts(array(
		"url" => 'http://'
	), $options));
	
	$widget = new SUNYIT_VTCalendar_Widget;
	$widget->widget(array(),array(
		'url' => $url
	));

}
add_shortcode( 'sunyit_vtcalendar', 'sunyit_vtcalendar_shortcode' );

function sunyit_vtcalendar_list_shortcode ( $options = array(), $content = null ) {
  
	extract(shortcode_atts(array(
		"url" => 'http://',
		"limit" => 5,
		"template" => false
	), $options));
  
	$widget = new SUNYIT_VTCalendar_Widget;
	return $widget->list_widget(array(),array(
		'url' => $url,
		'limit' => $limit,
		'template' => $content
	));

}
add_shortcode( 'sunyit_vtcalendar_list', 'sunyit_vtcalendar_list_shortcode' );

?>
